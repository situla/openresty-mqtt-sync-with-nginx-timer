-- пример из проекта luamqtt https://github.com/xHasKx/luamqtt/blob/master/examples/openresty/app/main-sync.lua
-- с использованием таймеров nginx - https://github.com/openresty/lua-nginx-module#ngxtimerat

-- luacheck: globals ngx
local log = ngx.log
local timer_at = ngx.timer.at
--local ERR = ngx.ERR
local ERR = ngx.INFO
local tbl_concat = table.concat

local function trace(...)
	local line = {}
	for i = 1, select("#", ...) do
		line[i] = tostring(select(i, ...))
	end
	log(ERR, tbl_concat(line, " "))
end

trace("main.lua started")

local start_timer

local function on_timer(...)
	trace("on_timer: ", ...)

	local mqtt = require("mqtt")

	-- create mqtt client
	local client = mqtt.client{
                uri = "192.168.1.97",
		clean = true,
		connector = require("mqtt.ngxsocket"),
		--secure = true, -- optional
	}
	trace("created MQTT client", client)

	client:on{
		connect = function(connack)
			if connack.rc ~= 0 then
				trace("connection to broker failed:", connack)
				return
			end
			trace("connected:", connack) -- successful connection

			-- subscribe to test topic and publish message after it
			assert(client:subscribe{ topic="/devices/mpu-adc-r0_171/controls/MPU-ADC Board Temp", qos=1, callback=function(suback)
				trace("subscribed, msu21s_10/MPU-ADC Board Temp:", suback)

				-- publish test message
				--trace('publishing test message "hello" to "luamqtt/simpletest" topic...')
				--assert(client:publish{
					--topic = "luamqtt/simpletest",
					--payload = "hello",
					--qos = 1
				--})
			end})

			-- subscribe second topic
			assert(client:subscribe{ topic="/devices/msu21s_10/controls/Illumination", qos=1, callback=function(suback)
				trace("subscribed, mpu-adc-r0_171/Illumination:", suback)

			end})

			-- subscribe one another
			assert(client:subscribe{ topic="/devices/mpu-adc-r0_171/controls/Input 11", qos=1, callback=function(suback)
				trace("subscribed, mpu-adc-r0_171/controls/Input 11:", suback)

			end})
		end,

		message = function(msg)
			assert(client:acknowledge(msg))

			trace("received:", msg)
		end,

		error = function(err)
			trace("MQTT client error:", err)
		end,

		close = function(conn)
			trace("MQTT conn closed:", conn.close_reason)
		end
	}

	trace("running client in synchronous input/output loop")
	mqtt.run_sync(client)
	trace("done, synchronous input/output loop is stopped")

	-- to reconnect
	--start_timer()
end

start_timer = function()
	local ok, err = timer_at(2, on_timer)
	if not ok then
		trace("failed to start timer:", err)
	end
end

start_timer()

